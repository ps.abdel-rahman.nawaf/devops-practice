#!/bin/bash

DATE=$(git log | grep Date | head -1 | awk '{print $2,$3,$4,$6}')
date=$(date -d"$DATE" +%y%m%d)
hash=$(git rev-parse --short=8  HEAD)
TAG="$date-$hash"

arg1=$("-Dspring.profiles.active=h2")
arg2=$("assignment-${tag}.jar")

docker build -t assiment:$TAG  --build-arg ${arg1}  --build-arg ${arg2} .
docker push abdalrahmanabuazzait/test-repo:$TAG
