FROM openjdk:8-jre-alpine3.9
ARG arg1
ARG arg2
ARG TAG
ENV A=${arg1}
ENV B=${arg2}
ENV ${TAG}
USER abd
WORKDIR /demo 
COPY target/assignment-$TAG.jar /demo
CMD java, -jar, $A, $B
